const express=require('express')
const routerPerson=require('./routes/person')

const app=express();


app.use(express.json())
app.use('/person',routerPerson)

app.listen(4000,'0.0.0.0',()=>{
    console.log('server started on port no 4000')
})