const express = require("express");
const db=require('../db')
const utils=require('../utils')

const router=express.Router();

router.get('/',(req,res)=>{
    const stat=`select * from person`;
    db.execute(stat,(error,data)=>{
        res.send(utils.createResult(error,data))
    })
})

router.post('/add',(req,res)=>{
    const{name,age,address,marks}=req.body
    const stat=`insert into person (name,age,address,marks) values('${name}', '${age}' ,'${address}', '${marks}')`;
    db.execute(stat,(error,data)=>{
        res.send(utils.createResult(error,data))
    })
})

router.delete('/delete',(req,res)=>{
    const{id}=req.body
    const stat=`delete from person where id='${id}'`;
    db.execute(stat,(error,data)=>{
        res.send(utils.createResult(error,data))
    })
})

router.put('/update',(req,res)=>{
    const{id,name}=req.body
    const stat=`update person set name ='${name}' where id='${id}'`;
    db.execute(stat,(error,data)=>{
        res.send(utils.createResult(error,data))
    })
})

module.exports=router